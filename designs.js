// Select color input
// Select size input

// When size is submitted by the user, call makeGrid()

function makeGrid() 
{
  //alert("I will delete the table");
  var table = document.getElementById('GridID');
  table.innerHTML="";     //all items inside object tables
  var N= document.getElementById('inputHeight').value;
  var M= document.getElementById('inputWidth').value;
  var tabledata='';
  for (var i = 0; i < N; i++) 
  {
  	tabledata +='<tr>';
  	for (j = 0; j<M; j++) {
  		tabledata+='<td></td>';
  	}
  	tabledata+='</tr>';
  }
  table.innerHTML=tabledata;
  AddEventForID();
}

function AddEventForID()
{
  var tds= document.getElementsByTagName('td');
  for (var x=0; x<tds.length ; x++)
   {
  	 tds[x].addEventListener("click",function(event){
  	 var current_td=event.target;
  	 current_td.style.backgroundColor=document.getElementById('colorPicker').value;})
   }
}
document.addEventListener('DOMContentLoaded',function(){
	AddEventForID();
}
);